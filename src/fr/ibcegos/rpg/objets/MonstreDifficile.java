package fr.ibcegos.rpg.objets;

public class MonstreDifficile extends Monstre {
	
	private final int ATTAQUE_MAGIQUE = 5;
	private final int ECHEC_ATTAQUE_MAGIQUE = 6;
	public MonstreDifficile() {
		super();
	}
	
	public void attaquer(Joueur joueur) {
		super.attaquer(joueur); // attaque de base du monstre
		// on passe � l'attaque magique
		De de = new De();
		int jetMonstre = de.genererNombreAleaEntre1et6();
		if (jetMonstre != ECHEC_ATTAQUE_MAGIQUE) {
			joueur.subirDegat(jetMonstre * ATTAQUE_MAGIQUE);
		}
	}

}
